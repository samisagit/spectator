FROM golang:1.13 AS builder

ENV GO111MODULE on

WORKDIR /module

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o binary ./main

FROM scratch

WORKDIR /bin

COPY --from=builder /module/binary ./
