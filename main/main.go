package main

import (
	"bufio"
	"context"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func main() {
	dir := os.Getenv("DIR_WATCH")
	cmd := os.Getenv("COMMAND")
	args := strings.Split(os.Getenv("ARGS"), "_")

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	cwc, kill := context.WithCancel(context.Background())

	commandChan := make(chan error)

	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				log.Println("event:", event)
				if event.Op&fsnotify.Write == fsnotify.Write {
					log.Println("killing current routine")
					kill()
					log.Println("resetting context")
					cwc, kill = context.WithCancel(context.Background())

					go runCommand(cwc, commandChan, cmd, args...)
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			case err = <-commandChan:
				if err != nil {
					log.Println("error: ", err)
					continue
				}
			}

		}
	}()

	i := 0 //
	err = filepath.Walk(dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if i == 0 && info.IsDir() { // root directory
				fmt.Printf("watching %s\n", path)
				err = watcher.Add(fmt.Sprintf("%s", path))
				if err != nil {
					log.Fatal(err)
				}

				return nil
			}
			if info.IsDir() {
				i++
				fmt.Printf("watching %s%s\n", path, info.Name())
				err = watcher.Add(fmt.Sprintf("%s%s", path, info.Name()))
				if err != nil {
					log.Fatal(err)
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}

	<-done
}

func runCommand(ctx context.Context, pipe chan error, command string, args ...string) {
	fmt.Printf("commmand -> %s %s\n", command, args)

	cmd := exec.CommandContext(ctx, command, args...)
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()

	err := cmd.Start()
	if err != nil {
		pipe <- err
		return
	}

	outScanner := bufio.NewScanner(stdout)
	outScanner.Split(bufio.ScanWords)
	for outScanner.Scan() {
		m := outScanner.Text()
		fmt.Println(m)
	}

	errScanner := bufio.NewScanner(stderr)
	errScanner.Split(bufio.ScanLines)
	for errScanner.Scan() {
		m := errScanner.Text()
		fmt.Println(m)
	}

	cmd.Wait()

	pipe <- err
}